import "./App.css";
import CanvasDrei from "./components/CanvasDrei";
// import CanvasMesh from "./components/CanvasMesh";

const App = () => {
  return (
    <div className="App">
      {/* <CanvasMesh /> */}
      <CanvasDrei />
    </div>
  );
};

export default App;
