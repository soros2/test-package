import { useCallback, useRef, useState, Suspense } from "react";
import { Canvas } from "@react-three/fiber";
import { OrbitControls, Edges, Text, Line } from "@react-three/drei";
import * as THREE from "three";
import Data from "./Data";

const Box = ({
  size = [1, 1, 1],
  color = "black",
  position = [0, 0, 0],
  edgeColor = "white",
}) => {
  const mesh = useRef(null);

  return (
    <mesh ref={mesh} position={position}>
      <boxBufferGeometry attach="geometry" args={size} />
      <meshStandardMaterial attach="material" color={color} />
      <Edges color={edgeColor} />
    </mesh>
  );
};

const TextLabel = ({
  position,
  label,
  rotateX,
  rotateY,
  rotateZ,
  lineSide,
  boxSize,
}) => {
  const measuredRef = useCallback(
    (node) => {
      if (node !== null) {
        node.rotation.x = rotateX || 0;
        node.rotation.y = rotateY || 0;
        node.rotation.z = rotateZ || 0;
      }
    },
    [rotateX, rotateY, rotateZ]
  );

  return (
    <>
      <Text
        ref={measuredRef}
        color="black"
        position={position}
        scale={[1, 1, 1]}
      >
        {label}
      </Text>
      {lineSide && boxSize && (
        <>
          <Line
            points={
              lineSide === "x"
                ? [
                    [-(boxSize.x / 2), 0, boxSize.z / 2 + 0.1],
                    [0 - label.length / 30, 0, boxSize.z / 2 + 0.1],
                  ]
                : lineSide === "z"
                ? [
                    [boxSize.x / 2 + 0.1, 0, boxSize.z / 2],
                    [boxSize.x / 2 + 0.1, 0, 0 + label.length / 30],
                  ]
                : lineSide === "y"
                ? [
                    [boxSize.x / 1.75, 0, -(boxSize.z / 2 + 0.1)],
                    [
                      boxSize.x / 1.75,
                      boxSize.y / 2 - 0.1,
                      -(boxSize.z / 2 + 0.1),
                    ],
                  ]
                : [0, 0, 0]
            }
            lineWidth={1}
          />
          <Line
            points={
              lineSide === "x"
                ? [
                    [0 + label.length / 30, 0, boxSize.z / 2 + 0.1],
                    [boxSize.x / 2, 0, boxSize.z / 2 + 0.1],
                  ]
                : lineSide === "z"
                ? [
                    [boxSize.x / 2 + 0.1, 0, 0 - label.length / 30],
                    [boxSize.x / 2 + 0.1, 0, -(boxSize.z / 2)],
                  ]
                : lineSide === "y"
                ? [
                    [
                      boxSize.x / 1.75,
                      boxSize.y / 2 + 0.1,
                      -(boxSize.z / 2 + 0.1),
                    ],
                    [boxSize.x / 1.75, boxSize.y, -(boxSize.z / 2 + 0.1)],
                  ]
                : [0, 0, 0]
            }
            lineWidth={1}
          />
        </>
      )}
    </>
  );
};

const CanvasMesh = () => {
  const scale = 100;
  const [size, setSize] = useState({ x: 0, y: 0, z: 0 });

  const randomColor = () => {
    let letters = "0123456789ABCDEF";
    let color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  const measuredRef = useCallback((node) => {
    if (node !== null) {
      const box = new THREE.Box3().setFromObject(node);
      setSize(box.getSize(new THREE.Vector3()));
    }
  }, []);

  return (
    <>
      <Canvas
        style={{ width: "100%" }}
        camera={{ position: [3, 1, 10], fov: 30 }}
      >
        <ambientLight intensity={0.5} />
        {/* <primitive object={new THREE.AxesHelper(100)} /> */}
        <OrbitControls />
        <group ref={measuredRef} position={[-(size.x / 2), 0, -(size.z / 2)]}>
          {Data.map(({ x, y, z, w, h, d }) => (
            <Box
              position={[
                (x + w / 2) / scale,
                (z + h / 2) / scale,
                (y + d / 2) / scale,
              ]}
              size={[w / scale, h / scale, d / scale]}
              color={randomColor()}
            />
          ))}
        </group>
        <Suspense>
          <TextLabel
            position={[0, 0, size.z / 2 + 0.1]}
            label={`${parseFloat(size.x * scale).toFixed(2)}`}
            boxSize={size}
            lineSide="x"
          />
          <TextLabel
            position={[size.x / 2 + 0.1, 0, 0]}
            label={`${parseFloat(size.z * scale).toFixed(2)}`}
            rotateY={1.6}
            boxSize={size}
            lineSide="z"
          />
          <TextLabel
            position={[size.x / 1.75, size.y / 2, -(size.z / 2 + 0.1)]}
            label={`${parseFloat(size.y * scale).toFixed(2)}`}
            boxSize={size}
            lineSide="y"
          />
        </Suspense>
      </Canvas>
    </>
  );
};

export default CanvasMesh;
