import { useRef } from "react";
import { Canvas } from "@react-three/fiber";
import { OrbitControls, Edges } from "@react-three/drei";

const Box = ({
  size = [1, 1, 1],
  color = "black",
  position = [0, 0, 0],
  edgeColor = "white",
}) => {
  const mesh = useRef(null);

  return (
    <mesh ref={mesh} position={position}>
      <boxBufferGeometry attach="geometry" args={size} />
      <meshStandardMaterial attach="material" color={color} />
      <Edges color={edgeColor} />
    </mesh>
  );
};

const CanvasMesh = () => {
  return (
    <Canvas style={{ width: "100%" }}>
      <OrbitControls />
      <ambientLight intensity={0.5} />
      <Box position={[0, 0, 0]} size={[2, 0.2, 2]} color="#c9c9c9" />
      <Box position={[0, 0.3, 0.5]} size={[2, 0.4, 1]} color="#57e0ff" />
      <Box position={[0, 0.2, -0.5]} size={[2, 0.2, 1]} color="#6257ff" />
      <Box position={[0, 0.4, -0.5]} size={[2, 0.2, 1]} color="#1fdb4e" />
      <Box position={[0, 0.65, 0.5]} size={[2, 0.3, 1]} color="#faaf23" />
      <Box position={[-0.5, 0.6, -0.5]} size={[1, 0.2, 1]} color="#fa4343" />
    </Canvas>
  );
};

export default CanvasMesh;
